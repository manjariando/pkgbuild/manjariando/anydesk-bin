# Maintainer: Nico <d3sox at protonmail dot com>

pkgname=anydesk-bin
pkgver=6.1.1
pkgrel=5.2
pkgdesc="The Fast Remote Desktop Application"
arch=('x86_64')
url="https://anydesk.com"
license=('custom')
makedepends=('patchelf' 'imagemagick')
conflicts=('anydesk')
provides=('anydesk')
replaces=('anydesk-debian')
options=('!strip')

source=("https://download.anydesk.com/linux/anydesk-${pkgver}-amd64.tar.gz"
        "https://metainfo.manjariando.com.br/${pkgname%%-bin}/com.${pkgname%%-bin}.metainfo.xml")

sha256sums=('102e72c75502a4779083320322dd047e2b0c00a25ead7444a00aad1db54325aa'
            'fd2cf29f0be42ddf85325f12ef1cd99bb222acdde01ee4575b58d3dd4a50a0c7')

package() {
    depends=('fakeroot' 'minizip' 'python-shiboken2' 'gtkglext' 'libglvnd' 'gtk2' 'libx11' 'glibc' 'glib2' 'gdk-pixbuf2' 'libxcb' 'cairo' 'pango' 'libxi' 'libxrender' 'libxrandr' 'libxtst' 'libxext' 'libxfixes' 'libxdamage' 'libxkbfile' 'gcc-libs' 'lsb-release' 'polkit')
    optdepends=('libpulse: audio support' 'gnome-themes-extra: adwaita theme')

    # install binary
    install -Dm755 "${srcdir}/anydesk-${pkgver}/anydesk" "${pkgdir}/usr/bin/anydesk"
    # patch the binary to replace obsolete dependency
    patchelf --replace-needed 'libpangox-1.0.so.0' 'libpangoxft-1.0.so' "${pkgdir}/usr/bin/anydesk"

    # install desktop entry
    install -Dm644 "${srcdir}/anydesk-${pkgver}/${pkgname%%-bin}.desktop" "${pkgdir}/usr/share/applications/com.anydesk.desktop"

    # install polkit action
    install -Dm644 "${srcdir}/anydesk-${pkgver}/polkit-1/com.anydesk.anydesk.policy" "${pkgdir}/usr/share/polkit-1/actions/com.anydesk.anydesk.policy"
    # install icon
    install -Dm644 "${srcdir}/anydesk-${pkgver}/icons/hicolor/scalable/apps/${pkgname%%-bin}.svg" "${pkgdir}/usr/share/pixmaps/${pkgname%%-bin}.svg"

    for size in 22 24 32 48 64 128 256; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/anydesk-${pkgver}/icons/hicolor/256x256/apps/${pkgname%%-bin}.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname%%-bin}.png"
    done

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname%%-bin}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname%%-bin}.metainfo.xml"

    # install license
    install -Dm644 "${srcdir}/anydesk-${pkgver}/copyright" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"

    # install systemd service
    install -Dm644 "${srcdir}/anydesk-${pkgver}/systemd/${pkgname%%-bin}.service" "${pkgdir}/usr/lib/systemd/system/${pkgname%%-bin}.service"
}
